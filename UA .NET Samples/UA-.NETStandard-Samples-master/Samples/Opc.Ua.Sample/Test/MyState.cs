using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Opc.Ua;

namespace test
{
    public partial class MyState
    {
        private ISystemContext m_simulationContext;

        Timer _timer;
        uint count = 0;

        #region Initialization
        /// <summary>
        /// Initializes the object as a collection of counters which change value on read.
        /// </summary>
        protected override void OnAfterCreate(ISystemContext context, NodeState node)
        {
            base.OnAfterCreate(context, node);

            this.Simulation.OnAfterTransition = OnControlSimulation;

            MySimulation.MyMethod.StateChanged += MyMethodStateChanged;
            MySimulation.MyMethod.OnCallMethod += OnCallMethod;
            MySimulation.MyMethod.OnCallMethod2 += OnCallMethod2;

        }

        private ServiceResult OnCallMethod2(ISystemContext context, MethodState method, NodeId objectId, IList<object> inputArguments, IList<object> outputArguments)
        {
            MyValue.Value = count;

            count++;

            this.ClearChangeMasks(context, true);

            return ServiceResult.Good;
        }

        private ServiceResult OnCallMethod(ISystemContext context, MethodState method, IList<object> inputArguments, IList<object> outputArguments)
        {
            return ServiceResult.Good;
        }

        private void MyMethodStateChanged(ISystemContext context, NodeState node, NodeStateChangeMasks changes)
        {
        }

        private ServiceResult OnControlSimulation(
            ISystemContext context,
            StateMachineState machine,
            uint transitionId,
            uint causeId,
            IList<object> inputArguments,
            IList<object> outputArguments)
        {
            switch (causeId)
            {
                case Opc.Ua.Methods.ProgramStateMachineType_Start:

                    if (_timer != null)
                    {
                        _timer.Dispose();
                        _timer = null;
                    }

                    _timer = new Timer(Simulation.UpdateRate.Value > 0 ? Simulation.UpdateRate.Value : 1000);
                    _timer.Elapsed += TimerElapsed;
                    _timer.AutoReset = true;
                    _timer.Start();

                    break;


                case Opc.Ua.Methods.ProgramStateMachineType_Halt:

                    break;

                case Opc.Ua.Methods.ProgramStateMachineType_Suspend:

                    if (_timer.Enabled)
                        _timer.Stop();

                    break;

                case Opc.Ua.Methods.ProgramStateMachineType_Resume:

                    if (!_timer.Enabled)
                        _timer.Start();

                    break;

                case Opc.Ua.Methods.ProgramStateMachineType_Reset:

                    _timer.Stop();
                    count = 0;
                    TimerElapsed(null, null);
                    break;


            }
            m_simulationContext = context;
            return ServiceResult.Good;
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            MyValue.Value = count;

            count++;

            this.ClearChangeMasks(m_simulationContext, true);
        }
        #endregion

        #region IDisposeable Methods
        /// <summary>
        /// Cleans up when the object is disposed.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //if (m_simulationTimer != null)
                //{
                //    m_simulationTimer.Dispose();
                //    m_simulationTimer = null;
                //}
            }
        }
        #endregion
    }
}
