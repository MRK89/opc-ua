/* ========================================================================
 * Copyright (c) 2005-2016 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Runtime.Serialization;
using Opc.Ua;

namespace test
{
    #region BoilerStateMachineState Class
    #if (!OPCUA_EXCLUDE_BoilerStateMachineState)
    /// <summary>
    /// Stores an instance of the BoilerStateMachineType ObjectType.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public partial class BoilerStateMachineState : ProgramStateMachineState
    {
        #region Constructors
        /// <summary>
        /// Initializes the type with its default attribute values.
        /// </summary>
        public BoilerStateMachineState(NodeState parent) : base(parent)
        {
        }

        /// <summary>
        /// Returns the id of the default type definition node for the instance.
        /// </summary>
        protected override NodeId GetDefaultTypeDefinitionId(NamespaceTable namespaceUris)
        {
            return Opc.Ua.NodeId.Create(test.ObjectTypes.BoilerStateMachineType, test.Namespaces.test, namespaceUris);
        }

        #if (!OPCUA_EXCLUDE_InitializationStrings)
        /// <summary>
        /// Initializes the instance.
        /// </summary>
        protected override void Initialize(ISystemContext context)
        {
            Initialize(context, InitializationString);
            InitializeOptionalChildren(context);
        }

        /// <summary>
        /// Initializes the instance with a node.
        /// </summary>
        protected override void Initialize(ISystemContext context, NodeState source)
        {
            InitializeOptionalChildren(context);
            base.Initialize(context, source);
        }

        /// <summary>
        /// Initializes the any option children defined for the instance.
        /// </summary>
        protected override void InitializeOptionalChildren(ISystemContext context)
        {
            base.InitializeOptionalChildren(context);
        }

        #region Initialization String
        private const string InitializationString =
           "AQAAAAsAAABNeU5hbWVzcGFjZf////8EYIAAAQAAAAEAHgAAAEJvaWxlclN0YXRlTWFjaGluZVR5cGVJ" +
           "bnN0YW5jZQEBnToBAZ06/////wsAAAAVYIkKAgAAAAAADAAAAEN1cnJlbnRTdGF0ZQEBnjoALwEAyAqe" +
           "OgAAABX/////AQH/////AgAAABVgiQoCAAAAAAACAAAASWQBAZ86AC4ARJ86AAAAEf////8BAf////8A" +
           "AAAAFWCJCgIAAAAAAAYAAABOdW1iZXIBAaE6AC4ARKE6AAAAB/////8BAf////8AAAAAFWCJCgIAAAAA" +
           "AA4AAABMYXN0VHJhbnNpdGlvbgEBozoALwEAzwqjOgAAABX/////AQH/////AwAAABVgiQoCAAAAAAAC" +
           "AAAASWQBAaQ6AC4ARKQ6AAAAEf////8BAf////8AAAAAFWCJCgIAAAAAAAYAAABOdW1iZXIBAaY6AC4A" +
           "RKY6AAAAB/////8BAf////8AAAAAFWCJCgIAAAAAAA4AAABUcmFuc2l0aW9uVGltZQEBpzoALgBEpzoA" +
           "AAEAJgH/////AQH/////AAAAABVgiQoCAAAAAAAJAAAARGVsZXRhYmxlAQGsOgAuAESsOgAAAAH/////" +
           "AQH/////AAAAABVgiQoCAAAAAAAKAAAAQXV0b0RlbGV0ZQEBrToALgBErToAAAAB/////wEB/////wAA" +
           "AAAVYIkKAgAAAAAADAAAAFJlY3ljbGVDb3VudAEBrjoALgBErjoAAAAG/////wEB/////wAAAAAkYYIK" +
           "BAAAAAEABQAAAFN0YXJ0AQHaOgMAAAAASwAAAENhdXNlcyB0aGUgUHJvZ3JhbSB0byB0cmFuc2l0aW9u" +
           "IGZyb20gdGhlIFJlYWR5IHN0YXRlIHRvIHRoZSBSdW5uaW5nIHN0YXRlLgAvAQB6Cdo6AAABAQEAAAAA" +
           "NQEBAco6AAAAACRhggoEAAAAAQAHAAAAU3VzcGVuZAEB2zoDAAAAAE8AAABDYXVzZXMgdGhlIFByb2dy" +
           "YW0gdG8gdHJhbnNpdGlvbiBmcm9tIHRoZSBSdW5uaW5nIHN0YXRlIHRvIHRoZSBTdXNwZW5kZWQgc3Rh" +
           "dGUuAC8BAHsJ2zoAAAEBAQAAAAA1AQEB0DoAAAAAJGGCCgQAAAABAAYAAABSZXN1bWUBAdw6AwAAAABP" +
           "AAAAQ2F1c2VzIHRoZSBQcm9ncmFtIHRvIHRyYW5zaXRpb24gZnJvbSB0aGUgU3VzcGVuZGVkIHN0YXRl" +
           "IHRvIHRoZSBSdW5uaW5nIHN0YXRlLgAvAQB8Cdw6AAABAQEAAAAANQEBAdI6AAAAACRhggoEAAAAAQAE" +
           "AAAASGFsdAEB3ToDAAAAAGAAAABDYXVzZXMgdGhlIFByb2dyYW0gdG8gdHJhbnNpdGlvbiBmcm9tIHRo" +
           "ZSBSZWFkeSwgUnVubmluZyBvciBTdXNwZW5kZWQgc3RhdGUgdG8gdGhlIEhhbHRlZCBzdGF0ZS4ALwEA" +
           "fQndOgAAAQEDAAAAADUBAQHMOgA1AQEB1DoANQEBAdg6AAAAACRhggoEAAAAAQAFAAAAUmVzZXQBAd46" +
           "AwAAAABKAAAAQ2F1c2VzIHRoZSBQcm9ncmFtIHRvIHRyYW5zaXRpb24gZnJvbSB0aGUgSGFsdGVkIHN0" +
           "YXRlIHRvIHRoZSBSZWFkeSBzdGF0ZS4ALwEAfgneOgAAAQEBAAAAADUBAQHIOgAAAAA1YIkKAgAAAAEA" +
           "CgAAAFVwZGF0ZVJhdGUBAd86AwAAAAAmAAAAVGhlIHJhdGUgYXQgd2hpY2ggdGhlIHNpbXVsYXRpb24g" +
           "cnVucy4ALgBE3zoAAAAH/////wMD/////wAAAAA=";
        #endregion
        #endif
        #endregion

        #region Public Properties
        /// <summary>
        /// The rate at which the simulation runs.
        /// </summary>
        public PropertyState<uint> UpdateRate
        {
            get
            {
                return m_updateRate;
            }

            set
            {
                if (!Object.ReferenceEquals(m_updateRate, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_updateRate = value;
            }
        }

        /// <summary>
        /// Causes the Program to transition from the Ready state to the Running state.
        /// </summary>
        public MethodState Start
        {
            get
            {
                return m_startMethod;
            }

            set
            {
                if (!Object.ReferenceEquals(m_startMethod, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_startMethod = value;
            }
        }

        /// <summary>
        /// Causes the Program to transition from the Running state to the Suspended state.
        /// </summary>
        public MethodState Suspend
        {
            get
            {
                return m_suspendMethod;
            }

            set
            {
                if (!Object.ReferenceEquals(m_suspendMethod, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_suspendMethod = value;
            }
        }

        /// <summary>
        /// Causes the Program to transition from the Suspended state to the Running state.
        /// </summary>
        public MethodState Resume
        {
            get
            {
                return m_resumeMethod;
            }

            set
            {
                if (!Object.ReferenceEquals(m_resumeMethod, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_resumeMethod = value;
            }
        }

        /// <summary>
        /// Causes the Program to transition from the Ready, Running or Suspended state to the Halted state.
        /// </summary>
        public MethodState Halt
        {
            get
            {
                return m_haltMethod;
            }

            set
            {
                if (!Object.ReferenceEquals(m_haltMethod, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_haltMethod = value;
            }
        }

        /// <summary>
        /// Causes the Program to transition from the Halted state to the Ready state.
        /// </summary>
        public MethodState Reset
        {
            get
            {
                return m_resetMethod;
            }

            set
            {
                if (!Object.ReferenceEquals(m_resetMethod, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_resetMethod = value;
            }
        }
        #endregion

        #region Overridden Methods
        /// <summary>
        /// Populates a list with the children that belong to the node.
        /// </summary>
        /// <param name="context">The context for the system being accessed.</param>
        /// <param name="children">The list of children to populate.</param>
        public override void GetChildren(
            ISystemContext context,
            IList<BaseInstanceState> children)
        {
            if (m_updateRate != null)
            {
                children.Add(m_updateRate);
            }

            if (m_startMethod != null)
            {
                children.Add(m_startMethod);
            }

            if (m_suspendMethod != null)
            {
                children.Add(m_suspendMethod);
            }

            if (m_resumeMethod != null)
            {
                children.Add(m_resumeMethod);
            }

            if (m_haltMethod != null)
            {
                children.Add(m_haltMethod);
            }

            if (m_resetMethod != null)
            {
                children.Add(m_resetMethod);
            }

            base.GetChildren(context, children);
        }

        /// <summary>
        /// Finds the child with the specified browse name.
        /// </summary>
        protected override BaseInstanceState FindChild(
            ISystemContext context,
            QualifiedName browseName,
            bool createOrReplace,
            BaseInstanceState replacement)
        {
            if (QualifiedName.IsNull(browseName))
            {
                return null;
            }

            BaseInstanceState instance = null;

            switch (browseName.Name)
            {
                case test.BrowseNames.UpdateRate:
                {
                    if (createOrReplace)
                    {
                        if (UpdateRate == null)
                        {
                            if (replacement == null)
                            {
                                UpdateRate = new PropertyState<uint>(this);
                            }
                            else
                            {
                                UpdateRate = (PropertyState<uint>)replacement;
                            }
                        }
                    }

                    instance = UpdateRate;
                    break;
                }

                case test.BrowseNames.Start:
                {
                    if (createOrReplace)
                    {
                        if (Start == null)
                        {
                            if (replacement == null)
                            {
                                Start = new MethodState(this);
                            }
                            else
                            {
                                Start = (MethodState)replacement;
                            }
                        }
                    }

                    instance = Start;
                    break;
                }

                case test.BrowseNames.Suspend:
                {
                    if (createOrReplace)
                    {
                        if (Suspend == null)
                        {
                            if (replacement == null)
                            {
                                Suspend = new MethodState(this);
                            }
                            else
                            {
                                Suspend = (MethodState)replacement;
                            }
                        }
                    }

                    instance = Suspend;
                    break;
                }

                case test.BrowseNames.Resume:
                {
                    if (createOrReplace)
                    {
                        if (Resume == null)
                        {
                            if (replacement == null)
                            {
                                Resume = new MethodState(this);
                            }
                            else
                            {
                                Resume = (MethodState)replacement;
                            }
                        }
                    }

                    instance = Resume;
                    break;
                }

                case test.BrowseNames.Halt:
                {
                    if (createOrReplace)
                    {
                        if (Halt == null)
                        {
                            if (replacement == null)
                            {
                                Halt = new MethodState(this);
                            }
                            else
                            {
                                Halt = (MethodState)replacement;
                            }
                        }
                    }

                    instance = Halt;
                    break;
                }

                case test.BrowseNames.Reset:
                {
                    if (createOrReplace)
                    {
                        if (Reset == null)
                        {
                            if (replacement == null)
                            {
                                Reset = new MethodState(this);
                            }
                            else
                            {
                                Reset = (MethodState)replacement;
                            }
                        }
                    }

                    instance = Reset;
                    break;
                }
            }

            if (instance != null)
            {
                return instance;
            }

            return base.FindChild(context, browseName, createOrReplace, replacement);
        }
        #endregion

        #region Private Fields
        private PropertyState<uint> m_updateRate;
        private MethodState m_startMethod;
        private MethodState m_suspendMethod;
        private MethodState m_resumeMethod;
        private MethodState m_haltMethod;
        private MethodState m_resetMethod;
        #endregion
    }
    #endif
    #endregion

    #region MySimulationState Class
    #if (!OPCUA_EXCLUDE_MySimulationState)
    /// <summary>
    /// Stores an instance of the MySimulationType ObjectType.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public partial class MySimulationState : BaseObjectState
    {
        #region Constructors
        /// <summary>
        /// Initializes the type with its default attribute values.
        /// </summary>
        public MySimulationState(NodeState parent) : base(parent)
        {
        }

        /// <summary>
        /// Returns the id of the default type definition node for the instance.
        /// </summary>
        protected override NodeId GetDefaultTypeDefinitionId(NamespaceTable namespaceUris)
        {
            return Opc.Ua.NodeId.Create(test.ObjectTypes.MySimulationType, test.Namespaces.test, namespaceUris);
        }

        #if (!OPCUA_EXCLUDE_InitializationStrings)
        /// <summary>
        /// Initializes the instance.
        /// </summary>
        protected override void Initialize(ISystemContext context)
        {
            Initialize(context, InitializationString);
            InitializeOptionalChildren(context);
        }

        /// <summary>
        /// Initializes the instance with a node.
        /// </summary>
        protected override void Initialize(ISystemContext context, NodeState source)
        {
            InitializeOptionalChildren(context);
            base.Initialize(context, source);
        }

        /// <summary>
        /// Initializes the any option children defined for the instance.
        /// </summary>
        protected override void InitializeOptionalChildren(ISystemContext context)
        {
            base.InitializeOptionalChildren(context);
        }

        #region Initialization String
        private const string InitializationString =
           "AQAAAAsAAABNeU5hbWVzcGFjZf////8EYIAAAQAAAAEAGAAAAE15U2ltdWxhdGlvblR5cGVJbnN0YW5j" +
           "ZQEBKjsBASo7/////wEAAAAEYYIKBAAAAAEACAAAAE15TWV0aG9kAQErOwAvAQErOys7AAABAf////8A" +
           "AAAA";
        #endregion
        #endif
        #endregion

        #region Public Properties
        /// <summary>
        /// A description for the MyMethod Method.
        /// </summary>
        public MethodState MyMethod
        {
            get
            {
                return m_myMethodMethod;
            }

            set
            {
                if (!Object.ReferenceEquals(m_myMethodMethod, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_myMethodMethod = value;
            }
        }
        #endregion

        #region Overridden Methods
        /// <summary>
        /// Populates a list with the children that belong to the node.
        /// </summary>
        /// <param name="context">The context for the system being accessed.</param>
        /// <param name="children">The list of children to populate.</param>
        public override void GetChildren(
            ISystemContext context,
            IList<BaseInstanceState> children)
        {
            if (m_myMethodMethod != null)
            {
                children.Add(m_myMethodMethod);
            }

            base.GetChildren(context, children);
        }

        /// <summary>
        /// Finds the child with the specified browse name.
        /// </summary>
        protected override BaseInstanceState FindChild(
            ISystemContext context,
            QualifiedName browseName,
            bool createOrReplace,
            BaseInstanceState replacement)
        {
            if (QualifiedName.IsNull(browseName))
            {
                return null;
            }

            BaseInstanceState instance = null;

            switch (browseName.Name)
            {
                case test.BrowseNames.MyMethod:
                {
                    if (createOrReplace)
                    {
                        if (MyMethod == null)
                        {
                            if (replacement == null)
                            {
                                MyMethod = new MethodState(this);
                            }
                            else
                            {
                                MyMethod = (MethodState)replacement;
                            }
                        }
                    }

                    instance = MyMethod;
                    break;
                }
            }

            if (instance != null)
            {
                return instance;
            }

            return base.FindChild(context, browseName, createOrReplace, replacement);
        }
        #endregion

        #region Private Fields
        private MethodState m_myMethodMethod;
        #endregion
    }
    #endif
    #endregion

    #region MyState Class
    #if (!OPCUA_EXCLUDE_MyState)
    /// <summary>
    /// Stores an instance of the MyType ObjectType.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public partial class MyState : BaseObjectState
    {
        #region Constructors
        /// <summary>
        /// Initializes the type with its default attribute values.
        /// </summary>
        public MyState(NodeState parent) : base(parent)
        {
        }

        /// <summary>
        /// Returns the id of the default type definition node for the instance.
        /// </summary>
        protected override NodeId GetDefaultTypeDefinitionId(NamespaceTable namespaceUris)
        {
            return Opc.Ua.NodeId.Create(test.ObjectTypes.MyType, test.Namespaces.test, namespaceUris);
        }

        #if (!OPCUA_EXCLUDE_InitializationStrings)
        /// <summary>
        /// Initializes the instance.
        /// </summary>
        protected override void Initialize(ISystemContext context)
        {
            Initialize(context, InitializationString);
            InitializeOptionalChildren(context);
        }

        /// <summary>
        /// Initializes the instance with a node.
        /// </summary>
        protected override void Initialize(ISystemContext context, NodeState source)
        {
            InitializeOptionalChildren(context);
            base.Initialize(context, source);
        }

        /// <summary>
        /// Initializes the any option children defined for the instance.
        /// </summary>
        protected override void InitializeOptionalChildren(ISystemContext context)
        {
            base.InitializeOptionalChildren(context);
        }

        #region Initialization String
        private const string InitializationString =
           "AQAAAAsAAABNeU5hbWVzcGFjZf////+EYIAAAQAAAAEADgAAAE15VHlwZUluc3RhbmNlAQGZOgEBmToB" +
           "/////wMAAAA1YIkKAgAAAAEABwAAAE15VmFsdWUBAZo6AwAAAAARAAAASWwgbWlvIGJlbCB2YWxvcmUA" +
           "LwA/mjoAAAAH/////wEB/////wAAAACEYIAKAQAAAAEACgAAAFNpbXVsYXRpb24BAeA6AC8BAZ064DoA" +
           "AAH/////CwAAABVgiQoCAAAAAAAMAAAAQ3VycmVudFN0YXRlAQHhOgAvAQDICuE6AAAAFf////8BAf//" +
           "//8CAAAAFWCJCgIAAAAAAAIAAABJZAEB4joALgBE4joAAAAR/////wEB/////wAAAAAVYIkKAgAAAAAA" +
           "BgAAAE51bWJlcgEB5DoALgBE5DoAAAAH/////wEB/////wAAAAAVYIkKAgAAAAAADgAAAExhc3RUcmFu" +
           "c2l0aW9uAQHmOgAvAQDPCuY6AAAAFf////8BAf////8DAAAAFWCJCgIAAAAAAAIAAABJZAEB5zoALgBE" +
           "5zoAAAAR/////wEB/////wAAAAAVYIkKAgAAAAAABgAAAE51bWJlcgEB6ToALgBE6ToAAAAH/////wEB" +
           "/////wAAAAAVYIkKAgAAAAAADgAAAFRyYW5zaXRpb25UaW1lAQHqOgAuAETqOgAAAQAmAf////8BAf//" +
           "//8AAAAAFWCJCgIAAAAAAAkAAABEZWxldGFibGUBAe46AC4ARO46AAAAAf////8BAf////8AAAAAFWCJ" +
           "CgIAAAAAAAoAAABBdXRvRGVsZXRlAQHvOgAuAETvOgAAAAH/////AQH/////AAAAABVgiQoCAAAAAAAM" +
           "AAAAUmVjeWNsZUNvdW50AQHwOgAuAETwOgAAAAb/////AQH/////AAAAADVgiQoCAAAAAQAKAAAAVXBk" +
           "YXRlUmF0ZQEB/zoDAAAAACYAAABUaGUgcmF0ZSBhdCB3aGljaCB0aGUgc2ltdWxhdGlvbiBydW5zLgAu" +
           "AET/OgAAAAf/////AwP/////AAAAACRhggoEAAAAAQAFAAAAU3RhcnQBAQA7AwAAAABLAAAAQ2F1c2Vz" +
           "IHRoZSBQcm9ncmFtIHRvIHRyYW5zaXRpb24gZnJvbSB0aGUgUmVhZHkgc3RhdGUgdG8gdGhlIFJ1bm5p" +
           "bmcgc3RhdGUuAC8BAdo6ADsAAAEB/////wAAAAAkYYIKBAAAAAEABwAAAFN1c3BlbmQBASI7AwAAAABP" +
           "AAAAQ2F1c2VzIHRoZSBQcm9ncmFtIHRvIHRyYW5zaXRpb24gZnJvbSB0aGUgUnVubmluZyBzdGF0ZSB0" +
           "byB0aGUgU3VzcGVuZGVkIHN0YXRlLgAvAQHbOiI7AAABAf////8AAAAAJGGCCgQAAAABAAYAAABSZXN1" +
           "bWUBASM7AwAAAABPAAAAQ2F1c2VzIHRoZSBQcm9ncmFtIHRvIHRyYW5zaXRpb24gZnJvbSB0aGUgU3Vz" +
           "cGVuZGVkIHN0YXRlIHRvIHRoZSBSdW5uaW5nIHN0YXRlLgAvAQHcOiM7AAABAf////8AAAAAJGGCCgQA" +
           "AAABAAQAAABIYWx0AQEkOwMAAAAAYAAAAENhdXNlcyB0aGUgUHJvZ3JhbSB0byB0cmFuc2l0aW9uIGZy" +
           "b20gdGhlIFJlYWR5LCBSdW5uaW5nIG9yIFN1c3BlbmRlZCBzdGF0ZSB0byB0aGUgSGFsdGVkIHN0YXRl" +
           "LgAvAQHdOiQ7AAABAf////8AAAAAJGGCCgQAAAABAAUAAABSZXNldAEBJTsDAAAAAEoAAABDYXVzZXMg" +
           "dGhlIFByb2dyYW0gdG8gdHJhbnNpdGlvbiBmcm9tIHRoZSBIYWx0ZWQgc3RhdGUgdG8gdGhlIFJlYWR5" +
           "IHN0YXRlLgAvAQHeOiU7AAABAf////8AAAAAhGCACgEAAAABAAwAAABNeVNpbXVsYXRpb24BASw7AC8B" +
           "ASo7LDsAAAH/////AQAAAARhggoEAAAAAQAIAAAATXlNZXRob2QBAS07AC8BASs7LTsAAAEB/////wAA" +
           "AAA=";
        #endregion
        #endif
        #endregion

        #region Public Properties
        /// <summary>
        /// Il mio bel valore
        /// </summary>
        public BaseDataVariableState<uint> MyValue
        {
            get
            {
                return m_myValue;
            }

            set
            {
                if (!Object.ReferenceEquals(m_myValue, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_myValue = value;
            }
        }

        /// <summary>
        /// A description for the Simulation Object.
        /// </summary>
        public BoilerStateMachineState Simulation
        {
            get
            {
                return m_simulation;
            }

            set
            {
                if (!Object.ReferenceEquals(m_simulation, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_simulation = value;
            }
        }

        /// <summary>
        /// A description for the MySimulation Object.
        /// </summary>
        public MySimulationState MySimulation
        {
            get
            {
                return m_mySimulation;
            }

            set
            {
                if (!Object.ReferenceEquals(m_mySimulation, value))
                {
                    ChangeMasks |= NodeStateChangeMasks.Children;
                }

                m_mySimulation = value;
            }
        }
        #endregion

        #region Overridden Methods
        /// <summary>
        /// Populates a list with the children that belong to the node.
        /// </summary>
        /// <param name="context">The context for the system being accessed.</param>
        /// <param name="children">The list of children to populate.</param>
        public override void GetChildren(
            ISystemContext context,
            IList<BaseInstanceState> children)
        {
            if (m_myValue != null)
            {
                children.Add(m_myValue);
            }

            if (m_simulation != null)
            {
                children.Add(m_simulation);
            }

            if (m_mySimulation != null)
            {
                children.Add(m_mySimulation);
            }

            base.GetChildren(context, children);
        }

        /// <summary>
        /// Finds the child with the specified browse name.
        /// </summary>
        protected override BaseInstanceState FindChild(
            ISystemContext context,
            QualifiedName browseName,
            bool createOrReplace,
            BaseInstanceState replacement)
        {
            if (QualifiedName.IsNull(browseName))
            {
                return null;
            }

            BaseInstanceState instance = null;

            switch (browseName.Name)
            {
                case test.BrowseNames.MyValue:
                {
                    if (createOrReplace)
                    {
                        if (MyValue == null)
                        {
                            if (replacement == null)
                            {
                                MyValue = new BaseDataVariableState<uint>(this);
                            }
                            else
                            {
                                MyValue = (BaseDataVariableState<uint>)replacement;
                            }
                        }
                    }

                    instance = MyValue;
                    break;
                }

                case test.BrowseNames.Simulation:
                {
                    if (createOrReplace)
                    {
                        if (Simulation == null)
                        {
                            if (replacement == null)
                            {
                                Simulation = new BoilerStateMachineState(this);
                            }
                            else
                            {
                                Simulation = (BoilerStateMachineState)replacement;
                            }
                        }
                    }

                    instance = Simulation;
                    break;
                }

                case test.BrowseNames.MySimulation:
                {
                    if (createOrReplace)
                    {
                        if (MySimulation == null)
                        {
                            if (replacement == null)
                            {
                                MySimulation = new MySimulationState(this);
                            }
                            else
                            {
                                MySimulation = (MySimulationState)replacement;
                            }
                        }
                    }

                    instance = MySimulation;
                    break;
                }
            }

            if (instance != null)
            {
                return instance;
            }

            return base.FindChild(context, browseName, createOrReplace, replacement);
        }
        #endregion

        #region Private Fields
        private BaseDataVariableState<uint> m_myValue;
        private BoilerStateMachineState m_simulation;
        private MySimulationState m_mySimulation;
        #endregion
    }
    #endif
    #endregion
}