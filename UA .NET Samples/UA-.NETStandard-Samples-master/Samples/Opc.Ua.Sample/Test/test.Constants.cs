/* ========================================================================
 * Copyright (c) 2005-2016 The OPC Foundation, Inc. All rights reserved.
 *
 * OPC Foundation MIT License 1.00
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 * The complete license agreement can be found here:
 * http://opcfoundation.org/License/MIT/1.00/
 * ======================================================================*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Xml;
using System.Runtime.Serialization;
using Opc.Ua;

namespace test
{
    #region Method Identifiers
    /// <summary>
    /// A class that declares constants for all Methods in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class Methods
    {
        /// <summary>
        /// The identifier for the BoilerStateMachineType_Start Method.
        /// </summary>
        public const uint BoilerStateMachineType_Start = 15066;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Suspend Method.
        /// </summary>
        public const uint BoilerStateMachineType_Suspend = 15067;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Resume Method.
        /// </summary>
        public const uint BoilerStateMachineType_Resume = 15068;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Halt Method.
        /// </summary>
        public const uint BoilerStateMachineType_Halt = 15069;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Reset Method.
        /// </summary>
        public const uint BoilerStateMachineType_Reset = 15070;

        /// <summary>
        /// The identifier for the MySimulationType_MyMethod Method.
        /// </summary>
        public const uint MySimulationType_MyMethod = 15147;

        /// <summary>
        /// The identifier for the MyType_Simulation_Start Method.
        /// </summary>
        public const uint MyType_Simulation_Start = 15104;

        /// <summary>
        /// The identifier for the MyType_Simulation_Suspend Method.
        /// </summary>
        public const uint MyType_Simulation_Suspend = 15138;

        /// <summary>
        /// The identifier for the MyType_Simulation_Resume Method.
        /// </summary>
        public const uint MyType_Simulation_Resume = 15139;

        /// <summary>
        /// The identifier for the MyType_Simulation_Halt Method.
        /// </summary>
        public const uint MyType_Simulation_Halt = 15140;

        /// <summary>
        /// The identifier for the MyType_Simulation_Reset Method.
        /// </summary>
        public const uint MyType_Simulation_Reset = 15141;

        /// <summary>
        /// The identifier for the MyType_MySimulation_MyMethod Method.
        /// </summary>
        public const uint MyType_MySimulation_MyMethod = 15149;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Start Method.
        /// </summary>
        public const uint MyInstance1_Simulation_Start = 15137;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Suspend Method.
        /// </summary>
        public const uint MyInstance1_Simulation_Suspend = 15142;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Resume Method.
        /// </summary>
        public const uint MyInstance1_Simulation_Resume = 15143;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Halt Method.
        /// </summary>
        public const uint MyInstance1_Simulation_Halt = 15144;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Reset Method.
        /// </summary>
        public const uint MyInstance1_Simulation_Reset = 15145;

        /// <summary>
        /// The identifier for the MyInstance1_MySimulation_MyMethod Method.
        /// </summary>
        public const uint MyInstance1_MySimulation_MyMethod = 15151;
    }
    #endregion

    #region Object Identifiers
    /// <summary>
    /// A class that declares constants for all Objects in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class Objects
    {
        /// <summary>
        /// The identifier for the MyType_Simulation Object.
        /// </summary>
        public const uint MyType_Simulation = 15072;

        /// <summary>
        /// The identifier for the MyType_MySimulation Object.
        /// </summary>
        public const uint MyType_MySimulation = 15148;

        /// <summary>
        /// The identifier for the MyInstance1 Object.
        /// </summary>
        public const uint MyInstance1 = 15003;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation Object.
        /// </summary>
        public const uint MyInstance1_Simulation = 15105;

        /// <summary>
        /// The identifier for the MyInstance1_MySimulation Object.
        /// </summary>
        public const uint MyInstance1_MySimulation = 15150;
    }
    #endregion

    #region ObjectType Identifiers
    /// <summary>
    /// A class that declares constants for all ObjectTypes in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class ObjectTypes
    {
        /// <summary>
        /// The identifier for the BoilerStateMachineType ObjectType.
        /// </summary>
        public const uint BoilerStateMachineType = 15005;

        /// <summary>
        /// The identifier for the MySimulationType ObjectType.
        /// </summary>
        public const uint MySimulationType = 15146;

        /// <summary>
        /// The identifier for the MyType ObjectType.
        /// </summary>
        public const uint MyType = 15001;
    }
    #endregion

    #region Variable Identifiers
    /// <summary>
    /// A class that declares constants for all Variables in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class Variables
    {
        /// <summary>
        /// The identifier for the BoilerStateMachineType_CurrentState_Id Variable.
        /// </summary>
        public const uint BoilerStateMachineType_CurrentState_Id = 15007;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_CurrentState_Number Variable.
        /// </summary>
        public const uint BoilerStateMachineType_CurrentState_Number = 15009;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_LastTransition_Id Variable.
        /// </summary>
        public const uint BoilerStateMachineType_LastTransition_Id = 15012;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_LastTransition_Number Variable.
        /// </summary>
        public const uint BoilerStateMachineType_LastTransition_Number = 15014;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_LastTransition_TransitionTime Variable.
        /// </summary>
        public const uint BoilerStateMachineType_LastTransition_TransitionTime = 15015;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_CreateSessionId Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_CreateSessionId = 15027;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_CreateClientName Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_CreateClientName = 15028;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_InvocationCreationTime Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_InvocationCreationTime = 15029;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastTransitionTime Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_LastTransitionTime = 15030;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodCall Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_LastMethodCall = 15031;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodSessionId Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_LastMethodSessionId = 15032;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodInputArguments Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_LastMethodInputArguments = 15033;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodOutputArguments Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_LastMethodOutputArguments = 15034;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodInputValues Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_LastMethodInputValues = 15035;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodOutputValues Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_LastMethodOutputValues = 15036;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodCallTime Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_LastMethodCallTime = 15037;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodReturnStatus Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ProgramDiagnostics_LastMethodReturnStatus = 15038;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Halted_StateNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_Halted_StateNumber = 15041;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Ready_StateNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_Ready_StateNumber = 15043;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Running_StateNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_Running_StateNumber = 15045;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Suspended_StateNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_Suspended_StateNumber = 15047;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_HaltedToReady_TransitionNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_HaltedToReady_TransitionNumber = 15049;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ReadyToRunning_TransitionNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ReadyToRunning_TransitionNumber = 15051;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_RunningToHalted_TransitionNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_RunningToHalted_TransitionNumber = 15053;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_RunningToReady_TransitionNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_RunningToReady_TransitionNumber = 15055;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_RunningToSuspended_TransitionNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_RunningToSuspended_TransitionNumber = 15057;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_SuspendedToRunning_TransitionNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_SuspendedToRunning_TransitionNumber = 15059;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_SuspendedToHalted_TransitionNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_SuspendedToHalted_TransitionNumber = 15061;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_SuspendedToReady_TransitionNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_SuspendedToReady_TransitionNumber = 15063;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ReadyToHalted_TransitionNumber Variable.
        /// </summary>
        public const uint BoilerStateMachineType_ReadyToHalted_TransitionNumber = 15065;

        /// <summary>
        /// The identifier for the BoilerStateMachineType_UpdateRate Variable.
        /// </summary>
        public const uint BoilerStateMachineType_UpdateRate = 15071;

        /// <summary>
        /// The identifier for the MyType_MyValue Variable.
        /// </summary>
        public const uint MyType_MyValue = 15002;

        /// <summary>
        /// The identifier for the MyType_Simulation_CurrentState Variable.
        /// </summary>
        public const uint MyType_Simulation_CurrentState = 15073;

        /// <summary>
        /// The identifier for the MyType_Simulation_CurrentState_Id Variable.
        /// </summary>
        public const uint MyType_Simulation_CurrentState_Id = 15074;

        /// <summary>
        /// The identifier for the MyType_Simulation_CurrentState_Number Variable.
        /// </summary>
        public const uint MyType_Simulation_CurrentState_Number = 15076;

        /// <summary>
        /// The identifier for the MyType_Simulation_LastTransition Variable.
        /// </summary>
        public const uint MyType_Simulation_LastTransition = 15078;

        /// <summary>
        /// The identifier for the MyType_Simulation_LastTransition_Id Variable.
        /// </summary>
        public const uint MyType_Simulation_LastTransition_Id = 15079;

        /// <summary>
        /// The identifier for the MyType_Simulation_LastTransition_Number Variable.
        /// </summary>
        public const uint MyType_Simulation_LastTransition_Number = 15081;

        /// <summary>
        /// The identifier for the MyType_Simulation_LastTransition_TransitionTime Variable.
        /// </summary>
        public const uint MyType_Simulation_LastTransition_TransitionTime = 15082;

        /// <summary>
        /// The identifier for the MyType_Simulation_Deletable Variable.
        /// </summary>
        public const uint MyType_Simulation_Deletable = 15086;

        /// <summary>
        /// The identifier for the MyType_Simulation_AutoDelete Variable.
        /// </summary>
        public const uint MyType_Simulation_AutoDelete = 15087;

        /// <summary>
        /// The identifier for the MyType_Simulation_RecycleCount Variable.
        /// </summary>
        public const uint MyType_Simulation_RecycleCount = 15088;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_CreateSessionId Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_CreateSessionId = 15090;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_CreateClientName Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_CreateClientName = 15091;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_InvocationCreationTime Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_InvocationCreationTime = 15092;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastTransitionTime Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_LastTransitionTime = 15093;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodCall Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_LastMethodCall = 15094;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodSessionId Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_LastMethodSessionId = 15095;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodInputArguments Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_LastMethodInputArguments = 15096;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodOutputArguments Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_LastMethodOutputArguments = 15097;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodInputValues Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_LastMethodInputValues = 15098;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodOutputValues Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_LastMethodOutputValues = 15099;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodCallTime Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_LastMethodCallTime = 15100;

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodReturnStatus Variable.
        /// </summary>
        public const uint MyType_Simulation_ProgramDiagnostics_LastMethodReturnStatus = 15101;

        /// <summary>
        /// The identifier for the MyType_Simulation_UpdateRate Variable.
        /// </summary>
        public const uint MyType_Simulation_UpdateRate = 15103;

        /// <summary>
        /// The identifier for the MyInstance1_MyValue Variable.
        /// </summary>
        public const uint MyInstance1_MyValue = 15004;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_CurrentState Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_CurrentState = 15106;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_CurrentState_Id Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_CurrentState_Id = 15107;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_CurrentState_Number Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_CurrentState_Number = 15109;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_LastTransition Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_LastTransition = 15111;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_LastTransition_Id Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_LastTransition_Id = 15112;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_LastTransition_Number Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_LastTransition_Number = 15114;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_LastTransition_TransitionTime Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_LastTransition_TransitionTime = 15115;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Deletable Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_Deletable = 15119;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_AutoDelete Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_AutoDelete = 15120;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_RecycleCount Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_RecycleCount = 15121;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_CreateSessionId Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_CreateSessionId = 15123;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_CreateClientName Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_CreateClientName = 15124;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_InvocationCreationTime Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_InvocationCreationTime = 15125;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastTransitionTime Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_LastTransitionTime = 15126;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodCall Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_LastMethodCall = 15127;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodSessionId Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_LastMethodSessionId = 15128;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodInputArguments Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_LastMethodInputArguments = 15129;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodOutputArguments Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_LastMethodOutputArguments = 15130;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodInputValues Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_LastMethodInputValues = 15131;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodOutputValues Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_LastMethodOutputValues = 15132;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodCallTime Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_LastMethodCallTime = 15133;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodReturnStatus Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_ProgramDiagnostics_LastMethodReturnStatus = 15134;

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_UpdateRate Variable.
        /// </summary>
        public const uint MyInstance1_Simulation_UpdateRate = 15136;
    }
    #endregion

    #region Method Node Identifiers
    /// <summary>
    /// A class that declares constants for all Methods in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class MethodIds
    {
        /// <summary>
        /// The identifier for the BoilerStateMachineType_Start Method.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_Start = new ExpandedNodeId(test.Methods.BoilerStateMachineType_Start, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Suspend Method.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_Suspend = new ExpandedNodeId(test.Methods.BoilerStateMachineType_Suspend, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Resume Method.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_Resume = new ExpandedNodeId(test.Methods.BoilerStateMachineType_Resume, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Halt Method.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_Halt = new ExpandedNodeId(test.Methods.BoilerStateMachineType_Halt, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Reset Method.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_Reset = new ExpandedNodeId(test.Methods.BoilerStateMachineType_Reset, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MySimulationType_MyMethod Method.
        /// </summary>
        public static readonly ExpandedNodeId MySimulationType_MyMethod = new ExpandedNodeId(test.Methods.MySimulationType_MyMethod, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_Start Method.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_Start = new ExpandedNodeId(test.Methods.MyType_Simulation_Start, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_Suspend Method.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_Suspend = new ExpandedNodeId(test.Methods.MyType_Simulation_Suspend, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_Resume Method.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_Resume = new ExpandedNodeId(test.Methods.MyType_Simulation_Resume, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_Halt Method.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_Halt = new ExpandedNodeId(test.Methods.MyType_Simulation_Halt, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_Reset Method.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_Reset = new ExpandedNodeId(test.Methods.MyType_Simulation_Reset, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_MySimulation_MyMethod Method.
        /// </summary>
        public static readonly ExpandedNodeId MyType_MySimulation_MyMethod = new ExpandedNodeId(test.Methods.MyType_MySimulation_MyMethod, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Start Method.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_Start = new ExpandedNodeId(test.Methods.MyInstance1_Simulation_Start, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Suspend Method.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_Suspend = new ExpandedNodeId(test.Methods.MyInstance1_Simulation_Suspend, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Resume Method.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_Resume = new ExpandedNodeId(test.Methods.MyInstance1_Simulation_Resume, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Halt Method.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_Halt = new ExpandedNodeId(test.Methods.MyInstance1_Simulation_Halt, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Reset Method.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_Reset = new ExpandedNodeId(test.Methods.MyInstance1_Simulation_Reset, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_MySimulation_MyMethod Method.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_MySimulation_MyMethod = new ExpandedNodeId(test.Methods.MyInstance1_MySimulation_MyMethod, test.Namespaces.test);
    }
    #endregion

    #region Object Node Identifiers
    /// <summary>
    /// A class that declares constants for all Objects in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class ObjectIds
    {
        /// <summary>
        /// The identifier for the MyType_Simulation Object.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation = new ExpandedNodeId(test.Objects.MyType_Simulation, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_MySimulation Object.
        /// </summary>
        public static readonly ExpandedNodeId MyType_MySimulation = new ExpandedNodeId(test.Objects.MyType_MySimulation, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1 Object.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1 = new ExpandedNodeId(test.Objects.MyInstance1, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation Object.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation = new ExpandedNodeId(test.Objects.MyInstance1_Simulation, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_MySimulation Object.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_MySimulation = new ExpandedNodeId(test.Objects.MyInstance1_MySimulation, test.Namespaces.test);
    }
    #endregion

    #region ObjectType Node Identifiers
    /// <summary>
    /// A class that declares constants for all ObjectTypes in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class ObjectTypeIds
    {
        /// <summary>
        /// The identifier for the BoilerStateMachineType ObjectType.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType = new ExpandedNodeId(test.ObjectTypes.BoilerStateMachineType, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MySimulationType ObjectType.
        /// </summary>
        public static readonly ExpandedNodeId MySimulationType = new ExpandedNodeId(test.ObjectTypes.MySimulationType, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType ObjectType.
        /// </summary>
        public static readonly ExpandedNodeId MyType = new ExpandedNodeId(test.ObjectTypes.MyType, test.Namespaces.test);
    }
    #endregion

    #region Variable Node Identifiers
    /// <summary>
    /// A class that declares constants for all Variables in the Model Design.
    /// </summary>
    /// <exclude />
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class VariableIds
    {
        /// <summary>
        /// The identifier for the BoilerStateMachineType_CurrentState_Id Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_CurrentState_Id = new ExpandedNodeId(test.Variables.BoilerStateMachineType_CurrentState_Id, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_CurrentState_Number Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_CurrentState_Number = new ExpandedNodeId(test.Variables.BoilerStateMachineType_CurrentState_Number, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_LastTransition_Id Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_LastTransition_Id = new ExpandedNodeId(test.Variables.BoilerStateMachineType_LastTransition_Id, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_LastTransition_Number Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_LastTransition_Number = new ExpandedNodeId(test.Variables.BoilerStateMachineType_LastTransition_Number, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_LastTransition_TransitionTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_LastTransition_TransitionTime = new ExpandedNodeId(test.Variables.BoilerStateMachineType_LastTransition_TransitionTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_CreateSessionId Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_CreateSessionId = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_CreateSessionId, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_CreateClientName Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_CreateClientName = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_CreateClientName, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_InvocationCreationTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_InvocationCreationTime = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_InvocationCreationTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastTransitionTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_LastTransitionTime = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_LastTransitionTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodCall Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_LastMethodCall = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_LastMethodCall, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodSessionId Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_LastMethodSessionId = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_LastMethodSessionId, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodInputArguments Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_LastMethodInputArguments = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_LastMethodInputArguments, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodOutputArguments Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_LastMethodOutputArguments = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_LastMethodOutputArguments, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodInputValues Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_LastMethodInputValues = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_LastMethodInputValues, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodOutputValues Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_LastMethodOutputValues = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_LastMethodOutputValues, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodCallTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_LastMethodCallTime = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_LastMethodCallTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ProgramDiagnostics_LastMethodReturnStatus Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ProgramDiagnostics_LastMethodReturnStatus = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ProgramDiagnostics_LastMethodReturnStatus, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Halted_StateNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_Halted_StateNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_Halted_StateNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Ready_StateNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_Ready_StateNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_Ready_StateNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Running_StateNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_Running_StateNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_Running_StateNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_Suspended_StateNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_Suspended_StateNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_Suspended_StateNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_HaltedToReady_TransitionNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_HaltedToReady_TransitionNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_HaltedToReady_TransitionNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ReadyToRunning_TransitionNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ReadyToRunning_TransitionNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ReadyToRunning_TransitionNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_RunningToHalted_TransitionNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_RunningToHalted_TransitionNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_RunningToHalted_TransitionNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_RunningToReady_TransitionNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_RunningToReady_TransitionNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_RunningToReady_TransitionNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_RunningToSuspended_TransitionNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_RunningToSuspended_TransitionNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_RunningToSuspended_TransitionNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_SuspendedToRunning_TransitionNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_SuspendedToRunning_TransitionNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_SuspendedToRunning_TransitionNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_SuspendedToHalted_TransitionNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_SuspendedToHalted_TransitionNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_SuspendedToHalted_TransitionNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_SuspendedToReady_TransitionNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_SuspendedToReady_TransitionNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_SuspendedToReady_TransitionNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_ReadyToHalted_TransitionNumber Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_ReadyToHalted_TransitionNumber = new ExpandedNodeId(test.Variables.BoilerStateMachineType_ReadyToHalted_TransitionNumber, test.Namespaces.test);

        /// <summary>
        /// The identifier for the BoilerStateMachineType_UpdateRate Variable.
        /// </summary>
        public static readonly ExpandedNodeId BoilerStateMachineType_UpdateRate = new ExpandedNodeId(test.Variables.BoilerStateMachineType_UpdateRate, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_MyValue Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_MyValue = new ExpandedNodeId(test.Variables.MyType_MyValue, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_CurrentState Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_CurrentState = new ExpandedNodeId(test.Variables.MyType_Simulation_CurrentState, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_CurrentState_Id Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_CurrentState_Id = new ExpandedNodeId(test.Variables.MyType_Simulation_CurrentState_Id, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_CurrentState_Number Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_CurrentState_Number = new ExpandedNodeId(test.Variables.MyType_Simulation_CurrentState_Number, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_LastTransition Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_LastTransition = new ExpandedNodeId(test.Variables.MyType_Simulation_LastTransition, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_LastTransition_Id Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_LastTransition_Id = new ExpandedNodeId(test.Variables.MyType_Simulation_LastTransition_Id, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_LastTransition_Number Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_LastTransition_Number = new ExpandedNodeId(test.Variables.MyType_Simulation_LastTransition_Number, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_LastTransition_TransitionTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_LastTransition_TransitionTime = new ExpandedNodeId(test.Variables.MyType_Simulation_LastTransition_TransitionTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_Deletable Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_Deletable = new ExpandedNodeId(test.Variables.MyType_Simulation_Deletable, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_AutoDelete Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_AutoDelete = new ExpandedNodeId(test.Variables.MyType_Simulation_AutoDelete, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_RecycleCount Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_RecycleCount = new ExpandedNodeId(test.Variables.MyType_Simulation_RecycleCount, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_CreateSessionId Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_CreateSessionId = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_CreateSessionId, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_CreateClientName Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_CreateClientName = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_CreateClientName, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_InvocationCreationTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_InvocationCreationTime = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_InvocationCreationTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastTransitionTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_LastTransitionTime = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_LastTransitionTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodCall Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_LastMethodCall = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_LastMethodCall, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodSessionId Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_LastMethodSessionId = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_LastMethodSessionId, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodInputArguments Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_LastMethodInputArguments = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_LastMethodInputArguments, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodOutputArguments Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_LastMethodOutputArguments = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_LastMethodOutputArguments, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodInputValues Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_LastMethodInputValues = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_LastMethodInputValues, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodOutputValues Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_LastMethodOutputValues = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_LastMethodOutputValues, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodCallTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_LastMethodCallTime = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_LastMethodCallTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_ProgramDiagnostics_LastMethodReturnStatus Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_ProgramDiagnostics_LastMethodReturnStatus = new ExpandedNodeId(test.Variables.MyType_Simulation_ProgramDiagnostics_LastMethodReturnStatus, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyType_Simulation_UpdateRate Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyType_Simulation_UpdateRate = new ExpandedNodeId(test.Variables.MyType_Simulation_UpdateRate, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_MyValue Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_MyValue = new ExpandedNodeId(test.Variables.MyInstance1_MyValue, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_CurrentState Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_CurrentState = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_CurrentState, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_CurrentState_Id Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_CurrentState_Id = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_CurrentState_Id, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_CurrentState_Number Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_CurrentState_Number = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_CurrentState_Number, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_LastTransition Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_LastTransition = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_LastTransition, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_LastTransition_Id Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_LastTransition_Id = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_LastTransition_Id, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_LastTransition_Number Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_LastTransition_Number = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_LastTransition_Number, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_LastTransition_TransitionTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_LastTransition_TransitionTime = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_LastTransition_TransitionTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_Deletable Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_Deletable = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_Deletable, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_AutoDelete Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_AutoDelete = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_AutoDelete, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_RecycleCount Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_RecycleCount = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_RecycleCount, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_CreateSessionId Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_CreateSessionId = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_CreateSessionId, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_CreateClientName Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_CreateClientName = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_CreateClientName, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_InvocationCreationTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_InvocationCreationTime = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_InvocationCreationTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastTransitionTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_LastTransitionTime = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_LastTransitionTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodCall Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_LastMethodCall = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_LastMethodCall, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodSessionId Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_LastMethodSessionId = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_LastMethodSessionId, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodInputArguments Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_LastMethodInputArguments = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_LastMethodInputArguments, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodOutputArguments Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_LastMethodOutputArguments = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_LastMethodOutputArguments, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodInputValues Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_LastMethodInputValues = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_LastMethodInputValues, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodOutputValues Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_LastMethodOutputValues = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_LastMethodOutputValues, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodCallTime Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_LastMethodCallTime = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_LastMethodCallTime, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_ProgramDiagnostics_LastMethodReturnStatus Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_ProgramDiagnostics_LastMethodReturnStatus = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_ProgramDiagnostics_LastMethodReturnStatus, test.Namespaces.test);

        /// <summary>
        /// The identifier for the MyInstance1_Simulation_UpdateRate Variable.
        /// </summary>
        public static readonly ExpandedNodeId MyInstance1_Simulation_UpdateRate = new ExpandedNodeId(test.Variables.MyInstance1_Simulation_UpdateRate, test.Namespaces.test);
    }
    #endregion

    #region BrowseName Declarations
    /// <summary>
    /// Declares all of the BrowseNames used in the Model Design.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class BrowseNames
    {
        /// <summary>
        /// The BrowseName for the BoilerStateMachineType component.
        /// </summary>
        public const string BoilerStateMachineType = "BoilerStateMachineType";

        /// <summary>
        /// The BrowseName for the Halt component.
        /// </summary>
        public const string Halt = "Halt";

        /// <summary>
        /// The BrowseName for the MyInstance1 component.
        /// </summary>
        public const string MyInstance1 = "MyInstance1";

        /// <summary>
        /// The BrowseName for the MyMethod component.
        /// </summary>
        public const string MyMethod = "MyMethod";

        /// <summary>
        /// The BrowseName for the MySimulation component.
        /// </summary>
        public const string MySimulation = "MySimulation";

        /// <summary>
        /// The BrowseName for the MySimulationType component.
        /// </summary>
        public const string MySimulationType = "MySimulationType";

        /// <summary>
        /// The BrowseName for the MyType component.
        /// </summary>
        public const string MyType = "MyType";

        /// <summary>
        /// The BrowseName for the MyValue component.
        /// </summary>
        public const string MyValue = "MyValue";

        /// <summary>
        /// The BrowseName for the Reset component.
        /// </summary>
        public const string Reset = "Reset";

        /// <summary>
        /// The BrowseName for the Resume component.
        /// </summary>
        public const string Resume = "Resume";

        /// <summary>
        /// The BrowseName for the Simulation component.
        /// </summary>
        public const string Simulation = "Simulation";

        /// <summary>
        /// The BrowseName for the Start component.
        /// </summary>
        public const string Start = "Start";

        /// <summary>
        /// The BrowseName for the Suspend component.
        /// </summary>
        public const string Suspend = "Suspend";

        /// <summary>
        /// The BrowseName for the UpdateRate component.
        /// </summary>
        public const string UpdateRate = "UpdateRate";
    }
    #endregion

    #region Namespace Declarations
    /// <summary>
    /// Defines constants for all namespaces referenced by the model design.
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Opc.Ua.ModelCompiler", "1.0.0.0")]
    public static partial class Namespaces
    {
        /// <summary>
        /// The URI for the test namespace (.NET code namespace is 'test').
        /// </summary>
        public const string test = "MyNamespace";

        /// <summary>
        /// The URI for the OpcUa namespace (.NET code namespace is 'Opc.Ua').
        /// </summary>
        public const string OpcUa = "http://opcfoundation.org/UA/";

        /// <summary>
        /// The URI for the OpcUaXsd namespace (.NET code namespace is 'Opc.Ua').
        /// </summary>
        public const string OpcUaXsd = "http://opcfoundation.org/UA/2008/02/Types.xsd";
    }
    #endregion
}