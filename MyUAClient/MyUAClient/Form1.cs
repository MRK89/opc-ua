﻿using Communication;
using Opc.Ua;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace MyUAClient
{
    public partial class Form1 : Form
    {
        private UAClient _uaClient;
        public Form1()
        {
            InitializeComponent();
        }

        private void ServerStateChanged(ServerState serverState)
        {
            if(serverState != ServerState.Running)
            {
                pnlStatus.BackColor = Color.Red;
                return;
            }

            pnlStatus.BackColor = Color.Green;
        }

        private void btn_Connect_Click(object sender, EventArgs e)
        {
            btn_Disconnect_Click(null, null);

            _uaClient = new UAClient();
            _uaClient.ServerStateChanged += ServerStateChanged;

            _uaClient.Connect(tB_IP.Text, tB_Port.Text);
        }

        private void btn_Browse_Click(object sender, EventArgs e)
        {
            if (!_uaClient.IsConnected())
                return;

            foreach (var name in _uaClient.GetItemsName())
            {
                var index = dgv.Rows.Add();
                dgv.Rows[index].Cells["NodeId"].Value = name;
            }
        }

        private void dgv_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            (new ItemForm(dgv.Rows[e.RowIndex].Cells["NodeId"].Value.ToString(), _uaClient)).Show();
        }

        private void btn_Disconnect_Click(object sender, EventArgs e)
        {
            if (_uaClient != null)
                _uaClient.Disconnect();

            ServerStateChanged(ServerState.Unknown);

            _uaClient = null;
        }
    }
}
