﻿using System;

namespace Communication
{
    public class UpdatedEventArgs
    {
        private Object _oldValue;
        private Object _itemValue;
        private DateTime timeStamp;
        private string _quality;

        public object OldValue
        {
            get { return this._oldValue; }
            set { this._oldValue = value; }
        }

        public object ItemValue
        {
            get { return this._itemValue; }
            set { this._itemValue = value; }
        }

        public DateTime TimeStamp
        {
            get { return this.timeStamp; }
            set { this.timeStamp = value; }
        }

        public string Quality
        {
            get { return this._quality; }
            set { this._quality = value; }
        }


        public UpdatedEventArgs(Object oldValue, Object value, string quality, DateTime timeStamp)
        {
            this._oldValue = oldValue;
            this._itemValue = value;
            this.timeStamp = timeStamp;
            this._quality = quality;
        }
    }
}
