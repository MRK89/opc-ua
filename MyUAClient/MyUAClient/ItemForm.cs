﻿using Communication;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace MyUAClient
{
    public partial class ItemForm : Form
    {
        string _nodeId;
        IOPCClient _uaClient;
        IOPCItem _opcItem;
        public ItemForm(string nodeId, IOPCClient uaClient)
        {
            InitializeComponent();

            _nodeId = nodeId;
            _uaClient = uaClient;

            var items = uaClient.CreateItems(new List<string>() { nodeId });

            if (items.Any())
            {
                _opcItem = items.First();
                tB_Value.Text = _opcItem.Value.ToString();
                _opcItem.Updated += Updated;
            }
        }

        private void Updated(object sender, UpdatedEventArgs e)
        {
            this.Invoke(new MethodInvoker(delegate () {
                tB_Value.Text = e.ItemValue?.ToString();
            }));
        }

        private void ItemForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _uaClient.RemoveItems(new List<string>() { _nodeId });
        }

        private void btn_Write_Click(object sender, System.EventArgs e)
        {
            _opcItem.Write(tB_Value.Text);
        }
    }
}
