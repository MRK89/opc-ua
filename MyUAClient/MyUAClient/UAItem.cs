﻿using System;
using System.Threading.Tasks;
using Opc.Ua;
using Opc.Ua.Client;
using System.Runtime.InteropServices;

namespace Communication
{
    class UAItem : IOPCItem
    {
        public event UpdatedEventHandler Updated;

        #region Field

        private object valueLock = new object();

        private MonitoredItem _item;

        private object _oldValue;
        private object _value;
        private DateTime _timeStamp;
        private string _quality;

        private bool _nodeExist = true;
        private Type _type = null;

        private VariableNode _variableNode = null;

        #endregion 

        public UAItem(string nodeId)
        {
            ItemID = nodeId;
            _item = new MonitoredItem() { StartNodeId = nodeId };
            _item.SamplingInterval = 1;
            _item.QueueSize = 1;
            _item.DiscardOldest = true;
            _item.Notification += OnNotification;
        }

        #region interface methods

        public string ItemID { get; }

        public object Value
        {
            get
            {
                lock (valueLock)
                {
                    if (_value != null)
                        return _value;

                    _value = PrivateRead();
                    return _value;
                }
            }
            set
            {
                object oldValueForArgs;
                DateTime timeStampForArgs;

                lock (valueLock)
                {
                    _oldValue = _value;
                    oldValueForArgs = _value;

                    _value = value;

                    _timeStamp = _item.LastMessage.PublishTime;
                    timeStampForArgs = _timeStamp;
                }

                Updated?.Invoke(this, new UpdatedEventArgs(oldValueForArgs, value, "Good", timeStampForArgs));
            }
        }

        public string Quality => _quality;

        public void Write(object Value)
        {
            if (!CheckNode())
                return;

            var valueToWrite = new WriteValue();
            valueToWrite.NodeId = ItemID;
            valueToWrite.AttributeId = Attributes.Value;
            valueToWrite.Value.Value = Convert.ChangeType(Value, _type);
            valueToWrite.Value.StatusCode = StatusCodes.Good;
            valueToWrite.Value.ServerTimestamp = DateTime.MinValue;
            valueToWrite.Value.SourceTimestamp = DateTime.MinValue;

            var valuesToWrite = new WriteValueCollection();
            valuesToWrite.Add(valueToWrite);

            _item.Subscription.Session.BeginWrite(
                requestHeader: null,
                nodesToWrite: valuesToWrite,
                callback: asyncResult =>
                {
                    var response = _item.Subscription.Session.EndWrite(
                        result: asyncResult,
                        results: out StatusCodeCollection results,
                        diagnosticInfos: out DiagnosticInfoCollection diag);

                    //CheckReturnValue(response.ServiceResult);
                    //CheckReturnValue(results[0]);

                },
                asyncState: null);
        }

        public object Read()
        {
            return Value;
        }

        public void SetActive(bool active)
        {
            if (active)
            {
                _item.MonitoringMode = MonitoringMode.Reporting;
                return;
            }

            _item.MonitoringMode = MonitoringMode.Disabled;
        }

        public bool IsActive()
        {
            return _item.MonitoringMode == MonitoringMode.Reporting;
        }

        #endregion

        #region private methods

        private void OnNotification(MonitoredItem item, MonitoredItemNotificationEventArgs e)
        {
            foreach (DataValue value in item.DequeueValues())
            {
                _quality = value.StatusCode.ToString();
                Value = (Object)value.Value;
            }
        }

        private object PrivateRead()
        {
            if (!CheckNode())
                return null;

            var nodeToRead = new ReadValueId();
            nodeToRead.NodeId = ItemID;
            nodeToRead.AttributeId = Attributes.Value;

            var nodesToRead = new ReadValueIdCollection();
            nodesToRead.Add(nodeToRead);

            _item.Subscription.Session.Read(null, 0, TimestampsToReturn.Neither, nodesToRead, out DataValueCollection results, out DiagnosticInfoCollection diagnosticInfos);

            return results[0].Value;
        }

        private Boolean CheckNode()
        {
            if (!_nodeExist)
                return false;

            if (_variableNode != null)
                return true;

            _variableNode = GetVariableNode(ItemID);

            if (_variableNode != null)
            {
                _type = TypeInfo.GetSystemType(_variableNode.DataType, EncodeableFactory.GlobalFactory);
                return true;
            }

            _nodeExist = false;
            return false;
        }

        private VariableNode GetVariableNode(NodeId nodeId)
        {
            return _item.Subscription.Session.NodeCache.Find(nodeId) as VariableNode;
        }

        //private void CheckReturnValue(StatusCode status)
        //{
        //    if (!StatusCode.IsGood(status))
        //    {
        //        //logger.Error("WRITE FAILED");
        //    }
        //}

        #endregion

        #region Public Methods

        public MonitoredItem GetMonitoredItem
        {
            get => _item;

            set => _item = value;
        }

        #endregion 
    }
}
