﻿
using System.Collections.Generic;

namespace Communication
{
    public interface IOPCClient
    {
        void Connect(string ip, string port);

        void Disconnect();

        bool IsConnected();

        List<string> GetItemsName();

        List<IOPCItem> CreateItems(List<string> items);

        List<string> RemoveItems(List<string> items);

        void RemoveAll();
    }
}