﻿using System;
using System.Runtime.InteropServices;

namespace Communication
{

    public delegate void UpdatedEventHandler(object sender, UpdatedEventArgs e);

    public interface IOPCItem
    {
        event UpdatedEventHandler Updated;

        string ItemID { get; }

        object Value { get; }

        string Quality { get; }

        void Write(object Value);

        void SetActive(bool active);

        Boolean IsActive();

    }
}

