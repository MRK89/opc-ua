﻿using System;
using System.Collections.Generic;
using System.Linq;
using Opc.Ua;
using Opc.Ua.Client;
using System.Threading;
using System.Diagnostics;
using System.Windows.Forms;

namespace Communication
{
    public class UAClient : IOPCClient, IDisposable
    {
        #region Field

        public delegate void ServerStateChangedDelegate(ServerState serverState);
        public event ServerStateChangedDelegate ServerStateChanged;

        private const int SESSION_TIMEOUT = 60000;
        private const int KEEP_ALIVE_INTERVAL = 2000;
        private const int PUBLISHING_INTERVAL = 100;
        private const int KEEP_ALIVE_COUNT = 32;     //Il KEEP_ALIVE_COUNT * PUBLISHING_INTERVAL dovrebbe essere più grande del KEEP_ALIVE_INTERVAL
        private const int LIFETIME_COUNT = 100; //valore minimo del server a seconda del PUBLISH INTERVAL

        private Session session;
        private Subscription subscription;
        private ServerState serverState = ServerState.Unknown;

        private Browser browser;
        private List<String> itemsName = new List<string>();
        private Dictionary<String, IOPCItem> itemsDict = new Dictionary<String, IOPCItem>();

        #endregion

        public UAClient() { }

        public void Dispose()
        {
            try
            {
                if (session != null && session.Connected)
                {
                    this.session.KeepAlive -= SessionOnKeepAlive;
                    this.session.Close();
                    this.session.Dispose();
                }

                session = null;
                subscription.Dispose();
                browser = null;
                itemsDict.Clear();
            }
            catch
            { }
        }

        #region Interface Methods

        public void Connect(string ip, string port)
        {
            try
            {
                CreateSession(new Uri($"opc.tcp://{ip}:{port}"));
                CreateSubscription();

                if (session != null && subscription != null)
                {
                    session.AddSubscription(subscription);
                    subscription.Create();

                }
            }
            catch
            { }
        }

        public void Disconnect()
        {
            Dispose();
        }

        public bool IsConnected()
        {
            return (serverState == ServerState.Running);
        }

        public List<string> GetItemsName()
        {
            if (itemsName.Count == 0)
                Browse();

            return itemsName;
        }

        public IOPCItem CreateItem(String nodeId) { return null; }

        //Called from opcEngine to create all tags
        public List<IOPCItem> CreateItems(List<string> items)
        {
            List<IOPCItem> iOPCItemList = new List<IOPCItem>();
            List<MonitoredItem> monitoredItems = new List<MonitoredItem>();

            foreach (string item in items)
            {
                if (itemsDict.ContainsKey(item))
                    continue;

                UAItem newUAItem = new UAItem(item);
                iOPCItemList.Add(newUAItem);
                monitoredItems.Add(newUAItem.GetMonitoredItem);
                itemsDict.Add(item, newUAItem);
            }

            //invece di ritornare iOPCItemList, bisognerebbe ciclare sul dizionario
            //e ritornare solo gli item attivi...vedi AutomationServer o MarcomeServer
            subscription.AddItems(monitoredItems);
            subscription.ApplyChanges();

            //Thread.Sleep(2000); //Chapuza to subscription complete load

            return iOPCItemList;
        }

        public List<string> RemoveItems(List<string> items)
        {
            List<string> removed = new List<string>();

            foreach (string item in items)
            {
                try
                {
                    if (itemsDict.ContainsKey(item) && itemsDict[item] != null)
                    {
                        subscription.RemoveItem(((UAItem)itemsDict[item]).GetMonitoredItem);
                        subscription.ApplyChanges();
                        itemsDict.Remove(item);
                    }

                    removed.Add(item);
                }
                catch
                { }
            }

            return removed;
        }

        public void RemoveAll()
        {
            //subscription.RemoveItems(itemsDict.Select(x => ((UAItem)x.Value).GetMonitoredItem));
            //subscription.ApplyChanges();
            //itemsDict.Clear();
        }

        #endregion

        #region Private Methods

        private void CreateSession(Uri uri)
        {
            var appConfig = new ApplicationConfiguration();
            appConfig.ApplicationName = "MyApp1";
            appConfig.ApplicationType = ApplicationType.Client;
            appConfig.SecurityConfiguration.ApplicationCertificate = new CertificateIdentifier();
            appConfig.ClientConfiguration = new ClientConfiguration();
            appConfig.Validate(ApplicationType.Client);

            var discoveryClient = DiscoveryClient.Create(uri, null);

            var endPoints = discoveryClient.GetEndpoints(null);

            EndpointDescription endPoint = null;

            foreach (EndpointDescription endpointDescription in endPoints)
            {
                if (endpointDescription.SecurityMode == MessageSecurityMode.None)
                {
                    endPoint = endpointDescription;
                    break;
                }
            }

            endPoint.EndpointUrl = uri.ToString();

            var sc = new StringCollection();
            sc.Add(uri.ToString());
            endPoint.Server.DiscoveryUrls = sc;

            var confEndPoint = new ConfiguredEndpoint(null, endPoint);

            session = Session.Create(appConfig, confEndPoint, true, "", SESSION_TIMEOUT, null, null);

            session.KeepAliveInterval = KEEP_ALIVE_INTERVAL;

            session.KeepAlive += SessionOnKeepAlive;
            session.Notification += OnNotification;
        }

        Stopwatch s = new Stopwatch();
        private void OnNotification(Session session, NotificationEventArgs e)
        {
            s.Stop();
            Console.WriteLine($"NOTIFICA: {s.ElapsedMilliseconds} --- {e.NotificationMessage.NotificationData.Count}");
            s.Reset();
            s.Start();
        }

        private object lockServerState = new object();

        private void SessionOnKeepAlive(Session session, KeepAliveEventArgs e)
        {
            lock (lockServerState)
            {
                if (serverState != e.CurrentState)
                    ServerStateChanged.Invoke(e.CurrentState);

                serverState = e.CurrentState;
            }
        }

        private void CreateSubscription()
        {
            subscription = new Subscription(session.DefaultSubscription);
            subscription.PublishingInterval = PUBLISHING_INTERVAL;
            subscription.KeepAliveCount = KEEP_ALIVE_COUNT;
            subscription.LifetimeCount = LIFETIME_COUNT;
            subscription.MinLifetimeInterval = 0;
        }

        #endregion

        #region Browsing

        private void Browse()
        {
            browser = new Browser(session);
            browser.ReferenceTypeId = ReferenceTypeIds.HierarchicalReferences;

            INode node = browser.Session.NodeCache.Find(Objects.ObjectsFolder);

            RecursiveBrowse((NodeId)node.NodeId);
        }

        private void RecursiveBrowse(NodeId nodeId)
        {
            ReferenceDescriptionCollection refs = browser.Browse(nodeId);

            HashSet<ReferenceDescription> refDesList = new HashSet<ReferenceDescription>(refs.Where(x => x.NodeClass == NodeClass.Variable));
            itemsName.AddRange(refDesList.Select(x => x.NodeId.ToString()));
            refs.RemoveAll(x => refDesList.Contains(x));

            foreach (ReferenceDescription referenceDescription in refs)
            {

                if (referenceDescription.BrowseName.NamespaceIndex == 0)
                {
                    continue;
                }

                RecursiveBrowse((NodeId)referenceDescription.NodeId);
            }
        }

        public void ActiveGroup(bool active)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
